﻿using UnityEngine;

public class InputManager : MonoBehaviour
{

    public static InputManager instance = null;
    public float deadzoneValue = 0.011f;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            // Can't have more than one instance of this singleton
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public float GetThrottle()
    {
        var throttle = Mathf.Abs(Input.GetAxis("Throttle"));
        Debug.Log("throttle: " + throttle);
        return throttle;
    }

    public float GetSide()
    {
        float sideAxis = Input.GetAxis("Side");

        if (AxisDeadzoned(sideAxis))
        {
            return 0;
        }

        return sideAxis;
    }

    public float GetForward(bool invert = false)
    {
        float fwdAxis = Input.GetAxis("Forward");

        if (AxisDeadzoned(fwdAxis))
        {
            return 0;
        }

        if (invert)
        {
            fwdAxis *= -1;
        }

        return fwdAxis;
    }

    private bool AxisDeadzoned(float axisValue)
    {
        return Mathf.Abs(axisValue) < deadzoneValue;
    }

}
