﻿using UnityEngine;

public class FollowCam : MonoBehaviour
{

    public Transform target;

    public float backDistance;
    public float upDistance;
	
	void Update ()
    {
        Vector3 newPos = target.transform.position;
        newPos -= Vector3.forward * backDistance;
        
        newPos.y = target.transform.position.y + upDistance;

        transform.position = new Vector3(newPos.x, newPos.y, newPos.z);

        transform.LookAt(target);
    }
}
