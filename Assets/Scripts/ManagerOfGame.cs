﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerOfGame : MonoBehaviour
{
    public GameObject drone;

    private void Update()
    {
        if(Input.GetAxisRaw("Reset") > Mathf.Epsilon)
        {
            ResetDrone();
        }
    }

    void ResetDrone()
    {
        drone.GetComponent<Rigidbody>().velocity = Vector3.zero;
        drone.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        drone.transform.position = Vector3.zero + Vector3.up * 0.5f;
        drone.transform.rotation = Quaternion.Euler(0, 0, 0);
    }
}
