﻿using UnityEngine;

public class Telemetry : MonoBehaviour
{
    private Rigidbody rb;

    public float speed { get { return rb.velocity.magnitude; } }
    public float altitude { get { return transform.position.y; } }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
}
