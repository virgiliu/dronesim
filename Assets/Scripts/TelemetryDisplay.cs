﻿using UnityEngine;
using UnityEngine.UI;

public class TelemetryDisplay : MonoBehaviour
{
    public Telemetry telemetry;

    public Text altitudeDisplay;
    public Text speedDisplay;
    public Text debugDisplay;

    public MotorController motorController;
    public Rigidbody rigidbody;

    private void Update()
    {
        altitudeDisplay.text = string.Format("Altitude: {0:0.00} m", telemetry.altitude);

        speedDisplay.text = string.Format("Speed: {0:0.00} m/s", telemetry.speed > Mathf.Epsilon ? telemetry.speed : 0);

        debugDisplay.text = GetDebugText();
    }

    private string GetDebugText()
    {
        return string.Format(
            "Motor base power: {0}\n" +
            "Forward power base: {1}\n" +
            "Scaled power: {2}\n" +
            "Mass: {3}\n" +
            "Drag: {4}\n" +
            "Angular drag: {5}\n",
            motorController.motorBasePower,
            motorController.forwardPowerBase,
            motorController.scaledPower,
            rigidbody.mass,
            rigidbody.drag,
            rigidbody.angularDrag
        );

    }
}
