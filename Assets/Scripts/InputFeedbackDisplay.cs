﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFeedbackDisplay : MonoBehaviour
{
    public RectTransform directionIndicator;
    public RectTransform throttleIndicator;
    public Text directionValues;

    float directionGridSideWidth = 50;
    float throttleMaxHeight = 100;
    

    
    private void Update()
    {
        UpdateHeadingindicator();
        UpdateDirectionTextValues();
        UpdateThrottleIndicator();
    }

    private void UpdateDirectionTextValues()
    {
        directionValues.text = string.Format("X: {0}\nY: {1}", InputManager.instance.GetForward(), InputManager.instance.GetSide());
    }

    private void UpdateThrottleIndicator()
    {
        throttleIndicator.sizeDelta = new Vector2(throttleIndicator.sizeDelta.x, throttleMaxHeight * InputManager.instance.GetThrottle());
    }

    private void UpdateHeadingindicator()
    {
        directionIndicator.localPosition = new Vector3(
            directionGridSideWidth * InputManager.instance.GetSide(),
            directionGridSideWidth * InputManager.instance.GetForward(),
            0
        );
    }
}
