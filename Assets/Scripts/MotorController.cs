﻿using UnityEngine;

public class MotorController : MonoBehaviour
{
    public Motor FR; // Front-right
    public Motor FL; // Front-left
    public Motor BR; // Back-right
    public Motor BL; // Back-left

    public float motorBasePower = 2f;
    public float forwardPowerBase = 0.1f;
    public float sidePowerBase = 0.05f;


    /// <summary>
    /// Power scaled by player input.
    /// Can range between 0 and `motorBasePower`
    /// </summary>
    public float scaledPower { get; private set; }

    private void Update()
    {
        scaledPower = motorBasePower * InputManager.instance.GetThrottle();
        float forwardPowerOffset = forwardPowerBase * InputManager.instance.GetForward(true);
        float sidePowerOffset = sidePowerBase * InputManager.instance.GetSide();

        SetPowerForAllMotors(scaledPower, forwardPowerOffset, sidePowerOffset);
    }

    private void SetPowerForAllMotors(float power, float forwardPowerOffset, float sidePowerOffset)
    {
        FR.SetPower(power + forwardPowerOffset - sidePowerOffset);
        FL.SetPower(power + forwardPowerOffset + sidePowerOffset);
        BR.SetPower(power - forwardPowerOffset - sidePowerOffset);
        BL.SetPower(power - forwardPowerOffset + sidePowerOffset);
    }
}
