﻿using System;
using UnityEngine;

public class Motor : MonoBehaviour
{
    Rigidbody rb;

    [SerializeField]
    float power;

    Vector3 thrust;

    private void Start()
    {
        rb = transform.parent.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        thrust = transform.up * power;
        ApplyThrust();
    }

    private void ApplyThrust()
    {
        rb.AddForceAtPosition(thrust, transform.position);
    }

    public void SetPower(float newPower)
    {
        power = newPower;
    }
}
