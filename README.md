### Drone simulator

A **very** basic drone simulator. Doesn't have yaw (yet).

Requires game controller.

[High resolution demo](https://gfycat.com/hoarsehonorablebrownbutterfly)

![Flying demo](https://thumbs.gfycat.com/HoarseHonorableBrownbutterfly-size_restricted.gif)
